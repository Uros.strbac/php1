<html>

<head>
  <title>Domaci zadatak</title>
</head>

<body>

    <h4>Promenljiva tipa integer</h4>
    <?php
        $color = "#95f441";
        $int = 12345;
        echo "<span style='color:$color'> " .$int.  "</span>" ;
    ?>

    <h4>Promenljiva tipa string</h4>
    <?php
        $color = "#4286f4";
        $string = "ABCDEF";
        echo "<span style='color:$color'> " .$string.  "</span>" ;
    ?>

    <h4>Promenljiva tipa float</h4>
    <?php
        $color = "#f46441";
        $float = "123,456";
        echo "<span style='color:$color'> " .$float.  "</span>" ;
    ?>

    <h4>Promenljiva tipa boolean</h4>
    <?php
        $color="#41d9f4";
        $bool=TRUE;
        echo "<span style='color:$color'> " .$bool. "</span>";
    ?>
    <h4>Promenljiva tipa array</h4>
    <?php
        $color = "#f441d6";
        $array = array(0, 1, 2, 3, 4, 5, 6);
        foreach($array as $value){
        	echo "<span style='color:$color'> " .$value. "</span>";
        }
    ?>
    <p>Suma niza je</p>
   	<?php
   	      $color = "green";
   	      $niz = array(1,2,3,4,5,6);
   	      include 'function.php';
          echo colorSuma($color , $niz);
   	   ?>
</body>

</html>
